package com.example.contactsaver.util;

public class AppConstants {
    public static final String CONTACT_BUNDLE = "CONTACT_BUNDLE";
    public static final String CONTACTS_LIST_BUNDLE = "CONTACTS_LIST_BUNDLE";
}
