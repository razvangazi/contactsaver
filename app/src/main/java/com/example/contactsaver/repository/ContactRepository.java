package com.example.contactsaver.repository;

import com.example.contactsaver.interfaces.ContactRepositoryInterface;
import com.example.contactsaver.model.Category;
import com.example.contactsaver.model.CategoryContactsResponse;
import com.example.contactsaver.model.Contact;
import com.example.contactsaver.model.FullContactInfo;

import java.util.ArrayList;
import java.util.List;

//TODO: Remove the mocks and get the response objects from a service call
public class ContactRepository implements ContactRepositoryInterface {

    private static ContactRepository sInstance;

    public static ContactRepository getInstance() {
        if (sInstance == null) {
            sInstance = new ContactRepository();
        }
        return sInstance;
    }

    @Override
    public List<Category> getContactCategories() {
        return buildMockCategories();
    }

    @Override
    public List<CategoryContactsResponse> getCategoryContactResponse() {
        return buildMockCategoryContactResponseList();
    }

    @Override
    public FullContactInfo getFullContactInfo() {
        return buildMockFullContactInfo();
    }

    /**
     * Builds and returns a list of Category objects
     *
     * @return
     */
    private List<Category> buildMockCategories() {
        Category category1 = new Category();
        category1.setCategoryId("243caw41dd-64532f-32ff2-3f2412ffr-f43tr43t1");
        category1.setName("Work");
        category1.setIcon("ic_home_black_24dp");

        Category category2 = new Category();
        category2.setCategoryId("243caw41dd-64532f-32ff2-3f2412ffr-f43tr43t2");
        category2.setName("Home");
        category2.setIcon("ic_dashboard_black_24dp");

        Category category3 = new Category();
        category3.setCategoryId("243caw41dd-64532f-32ff2-3f2412ffr-f43tr43t3");
        category3.setName("Friends");
        category3.setIcon("ic_notifications_black_24dp");

        List<Category> categoryList = new ArrayList<>();
        categoryList.add(category1);
        categoryList.add(category2);
        categoryList.add(category3);

        return categoryList;
    }

    private List<CategoryContactsResponse> buildMockCategoryContactResponseList() {
        List<CategoryContactsResponse> categoryContactsResponses = new ArrayList<>();

        CategoryContactsResponse categoryContacts1 = new CategoryContactsResponse();
        CategoryContactsResponse categoryContacts2 = new CategoryContactsResponse();
        CategoryContactsResponse categoryContacts3 = new CategoryContactsResponse();

        Contact contact1 = new Contact();
        contact1.setContactId("1");
        contact1.setFirstName("Rob");
        contact1.setLastName("Atark");
        contact1.setAvatar("avagar");
        contact1.setCompany("Company ABC inc.");

        Contact contact2 = new Contact();
        contact2.setContactId("2");
        contact2.setFirstName("Sansa");
        contact2.setLastName("Ctark");
        contact2.setAvatar("avagar");
        contact2.setCompany("Company ABC inc.");

        Contact contact3 = new Contact();
        contact3.setContactId("3");
        contact3.setFirstName("Ned");
        contact3.setLastName("Ctark");
        contact3.setAvatar("avagar");
        contact3.setCompany("Company ABC inc.");

        Contact contact4 = new Contact();
        contact4.setContactId("4");
        contact4.setFirstName("Arya");
        contact4.setLastName("Stark");
        contact4.setAvatar("avagar");
        contact4.setCompany("Company ABC inc.");

        Contact contact5 = new Contact();
        contact5.setContactId("5");
        contact5.setFirstName("Raz");
        contact5.setLastName("Allen");
        contact5.setAvatar("avagar");
        contact5.setCompany("Company ABC inc.");

        Contact contact6 = new Contact();
        contact6.setContactId("6");
        contact6.setFirstName("John");
        contact6.setLastName("Stena");
        contact6.setAvatar("avagar");
        contact6.setCompany("Company ABC inc.");

        Contact contact7 = new Contact();
        contact7.setContactId("7");
        contact7.setFirstName("Sarah");
        contact7.setLastName("Parker");
        contact7.setAvatar("avagar");
        contact7.setCompany("Company ABC inc.");

        Contact contact8 = new Contact();
        contact8.setContactId("8");
        contact8.setFirstName("Harris");
        contact8.setLastName("Zu");
        contact8.setAvatar("avagar");
        contact8.setCompany("Company ABC inc.");

        Contact contact9 = new Contact();
        contact9.setContactId("7");
        contact9.setFirstName("Alice");
        contact9.setLastName("Dart");
        contact9.setAvatar("avagar");
        contact9.setCompany("Company ABC inc.");

        List<Contact> contacts1 = new ArrayList<>();
        List<Contact> contacts2 = new ArrayList<>();
        List<Contact> contacts3 = new ArrayList<>();
        contacts1.add(contact1);
        contacts1.add(contact2);
        contacts1.add(contact3);

        contacts2.add(contact4);
        contacts2.add(contact5);
        contacts2.add(contact6);

        contacts3.add(contact7);
        contacts3.add(contact8);
        contacts3.add(contact9);

        categoryContacts1.setCategoryId("243caw41dd-64532f-32ff2-3f2412ffr-f43tr43t1");
        categoryContacts1.setContacts(contacts1);

        categoryContacts2.setContacts(contacts2);
        categoryContacts2.setCategoryId("243caw41dd-64532f-32ff2-3f2412ffr-f43tr43t2");

        categoryContacts3.setContacts(contacts3);
        categoryContacts3.setCategoryId("243caw41dd-64532f-32ff2-3f2412ffr-f43tr43t3");

        categoryContactsResponses.add(categoryContacts1);
        categoryContactsResponses.add(categoryContacts2);
        categoryContactsResponses.add(categoryContacts3);

        return categoryContactsResponses;
    }

    private FullContactInfo buildMockFullContactInfo() {
        FullContactInfo fullContactInfo = new FullContactInfo();

        fullContactInfo.setContactId("1");
        fullContactInfo.setFirstName("Rob");
        fullContactInfo.setLastName("Stark");
        fullContactInfo.setAvatar("avagar");
        fullContactInfo.setCompany("Company ABC inc.");
        fullContactInfo.setUnreadMessages(10);

        return fullContactInfo;
    }
}
