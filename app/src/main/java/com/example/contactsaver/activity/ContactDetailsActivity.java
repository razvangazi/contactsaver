package com.example.contactsaver.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.TextView;

import com.example.contactsaver.R;
import com.example.contactsaver.model.Contact;
import com.example.contactsaver.util.AppConstants;
import com.example.contactsaver.viewmodel.ContactDetailsViewModel;

public class ContactDetailsActivity extends AppCompatActivity {

    private Contact mContact;
    private TextView tvContactName;
    private TextView tvCompanyName;
    private ContactDetailsViewModel mViewModel;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);

        mViewModel = ViewModelProviders.of(this).get(ContactDetailsViewModel.class);
        mViewModel.init();

        getBundleData();
        initComponent();
    }

    private void initComponent() {
        setLayoutViews();
        initContactName();
        initCompanyName();
    }


    private void initContactName() {
        if (mContact != null && !TextUtils.isEmpty(mContact.getFirstName()) && !TextUtils.isEmpty(mContact.getLastName())) {
            tvContactName.setText(mContact.getFirstName() + " " + mContact.getLastName());
        }
    }

    private void initCompanyName() {
        if (mContact != null && !TextUtils.isEmpty(mContact.getCompany())) {
            tvCompanyName.setText(mContact.getCompany());
        }
    }

    private void setLayoutViews() {
        tvContactName = findViewById(R.id.tv_contact_name);
        tvCompanyName = findViewById(R.id.tv_company_name);
    }

    private void getBundleData() {
        mContact = getIntent().getParcelableExtra(AppConstants.CONTACT_BUNDLE);
    }
}
