package com.example.contactsaver.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.contactsaver.R;
import com.example.contactsaver.adapter.ContactPagerAdapter;
import com.example.contactsaver.model.Category;
import com.example.contactsaver.viewmodel.ContactViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private ContactViewModel mViewModel;
    private ViewPager mContactViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        mViewModel.init();
        initComponent();
    }

    private void initComponent() {
        mTextMessage = (TextView) findViewById(R.id.message);
        initViewPager();
        initTabLayout();
    }

    private void initViewPager() {
        mContactViewPager = findViewById(R.id.vp_contacts);
        ContactPagerAdapter contactPagerAdapter = new ContactPagerAdapter(getSupportFragmentManager(), mViewModel.getContactCategories(), mViewModel.getCategoryContactsResponseList());
        mContactViewPager.setAdapter(contactPagerAdapter);
        mContactViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void initTabLayout() {
        TabLayout tabLayout = findViewById(R.id.tl_contacts);
        tabLayout.setupWithViewPager(mContactViewPager);
        setupTabIcons(tabLayout, mViewModel.getContactCategories());
    }

    private void setupTabIcons(TabLayout tabLayout, List<Category> categories) {
        for (int index = 0; index < categories.size(); index++) {
            int menuIcon = getResources().getIdentifier(categories.get(index).getIcon(), "drawable", getPackageName());
            tabLayout.getTabAt(index).setIcon(menuIcon);
        }
    }
}
