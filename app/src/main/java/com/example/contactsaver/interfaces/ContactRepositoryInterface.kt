package com.example.contactsaver.interfaces

import com.example.contactsaver.model.Category
import com.example.contactsaver.model.CategoryContactsResponse
import com.example.contactsaver.model.FullContactInfo

interface ContactRepositoryInterface {
    fun getContactCategories(): List<Category>
    fun getCategoryContactResponse(): List<CategoryContactsResponse>
    fun getFullContactInfo(): FullContactInfo
}