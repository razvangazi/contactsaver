package com.example.contactsaver.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.contactsaver.fragment.ContactsListFragment;
import com.example.contactsaver.model.Category;
import com.example.contactsaver.model.CategoryContactsResponse;

import java.util.List;

public class ContactPagerAdapter extends FragmentPagerAdapter {

    private List<Category> mCategories;
    private List<CategoryContactsResponse> mCategoryContactsResponses;

    public ContactPagerAdapter(FragmentManager fm, List<Category> categories, List<CategoryContactsResponse> categoryContactsResponses) {
        super(fm);
        mCategories = categories;
        mCategoryContactsResponses = categoryContactsResponses;
    }

    @Nullable
    @Override
    public Fragment getItem(int i) {
        Category category = mCategories.get(i);
        for (CategoryContactsResponse response : mCategoryContactsResponses) {
            if (response.getCategoryId().equals(category.getCategoryId())) {
                return ContactsListFragment.newInstance(response.getContacts());
            }
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mCategories.get(position).getName();
    }

    @Override
    public int getCount() {
        return mCategories.size();
    }
}
