package com.example.contactsaver.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.contactsaver.R;
import com.example.contactsaver.model.Contact;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListAdapter.ViewHolder> {

    private List<Contact> mContacts;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private String mLetterHolder = "";

    // data is passed into the constructor
    public ContactsListAdapter(Context context, List<Contact> contacts) {
        this.mInflater = LayoutInflater.from(context);
        this.mContacts = sortAlphabetically(contacts);
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.contacts_list_row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contact contact = mContacts.get(position);

        String contactLastNameLetter = contact.getLastName().substring(0, 1);

        if (!contactLastNameLetter.equals(mLetterHolder)) {
            mLetterHolder = contactLastNameLetter;
            holder.mLetter.setVisibility(View.VISIBLE);
            holder.mLetter.setText(mLetterHolder);
        } else {
            holder.mLetter.setVisibility(View.GONE);
        }

        holder.mContactName.setText(contact.getFirstName() + " " + contact.getLastName());
        holder.mCompanyName.setText(contact.getCompany());
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mContacts.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mLetter;
        TextView mContactName;
        TextView mCompanyName;

        ViewHolder(View itemView) {
            super(itemView);
            mLetter = itemView.findViewById(R.id.tv_letter);
            mContactName = itemView.findViewById(R.id.tv_contact_name);
            mCompanyName = itemView.findViewById(R.id.tv_company_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


    private List<Contact> sortAlphabetically(List<Contact> contacts) {
        if (contacts.size() > 0) {
            Collections.sort(contacts, new Comparator<Contact>() {
                @Override
                public int compare(final Contact object1, final Contact object2) {
                    return object1.getLastName().compareTo(object2.getLastName());
                }
            });
        }
        return contacts;
    }
}