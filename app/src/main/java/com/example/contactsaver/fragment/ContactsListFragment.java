package com.example.contactsaver.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.contactsaver.R;
import com.example.contactsaver.activity.ContactDetailsActivity;
import com.example.contactsaver.adapter.ContactsListAdapter;
import com.example.contactsaver.model.Contact;
import com.example.contactsaver.util.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class ContactsListFragment extends Fragment implements ContactsListAdapter.ItemClickListener {

    private RecyclerView mRvContactsList;
    private List<Contact> mContacts;

    public static ContactsListFragment newInstance(List<Contact> contacts) {
        ContactsListFragment fragment = new ContactsListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(AppConstants.CONTACTS_LIST_BUNDLE, (ArrayList<Contact>) contacts);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.contacts_list_fragment, null);
        getBundleData();
        initComponent(rootView);

        return rootView;
    }

    private void initComponent(View view) {
        initRecyclerView(view);
    }

    private void initRecyclerView(View view) {
        // set up the RecyclerView
        mRvContactsList = view.findViewById(R.id.rv_contacts_list);
        mRvContactsList.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvContactsList.setItemAnimator(new DefaultItemAnimator());

        ContactsListAdapter adapter = new ContactsListAdapter(getContext(), mContacts);
        adapter.setClickListener(this);
        mRvContactsList.setAdapter(adapter);
    }

    private void getBundleData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mContacts = bundle.getParcelableArrayList(AppConstants.CONTACTS_LIST_BUNDLE);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), ContactDetailsActivity.class);
        intent.putExtra(AppConstants.CONTACT_BUNDLE, mContacts.get(position));
        startActivity(intent);
    }
}
