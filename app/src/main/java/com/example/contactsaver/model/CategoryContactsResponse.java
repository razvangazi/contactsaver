package com.example.contactsaver.model;

import java.util.List;

public class CategoryContactsResponse {
    private String categoryId;
    private List<Contact> contacts;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return "CategoryContactsResponse{" +
                "categoryId='" + categoryId + '\'' +
                ", contacts=" + contacts +
                '}';
    }
}
