package com.example.contactsaver.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.example.contactsaver.model.FullContactInfo;
import com.example.contactsaver.repository.ContactRepository;

public class ContactDetailsViewModel extends ViewModel {
    private FullContactInfo mFullContactInfo;

    public void init() {
        mFullContactInfo = ContactRepository.getInstance().getFullContactInfo();
    }

    public FullContactInfo getFullContactInfo() {
        return mFullContactInfo;
    }
}
