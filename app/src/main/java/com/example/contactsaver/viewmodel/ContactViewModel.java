package com.example.contactsaver.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.example.contactsaver.model.Category;
import com.example.contactsaver.model.CategoryContactsResponse;
import com.example.contactsaver.repository.ContactRepository;

import java.util.List;

public class ContactViewModel extends ViewModel {

    private List<Category> mContactCategories;
    private List<CategoryContactsResponse> mCategoryContactsResponses;

    public void init() {
        mContactCategories = ContactRepository.getInstance().getContactCategories();
        mCategoryContactsResponses = ContactRepository.getInstance().getCategoryContactResponse();
    }

    public List<Category> getContactCategories() {
        return mContactCategories;
    }

    public List<CategoryContactsResponse> getCategoryContactsResponseList(){
        return mCategoryContactsResponses;
    }
}